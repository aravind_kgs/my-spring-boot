package com.coding.spring.userplaylist.pojos;

public class ArticlePlaylistPojo {

    private String articleId;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
